const mongoose = require("mongoose");

mongoose
    .connect('mongodb+srv://' + process.env.DB_USER_PASS + '@web-project.voe5nv8.mongodb.net/webProject')
    .then(() => console.log('Connecté à MongoDB'))
    .catch((err) => console.log("Échec de la connexion à MongoDB", err));