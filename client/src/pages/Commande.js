import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { CartContext } from '../components/CartContext';

const Commande = () => {
  const [categories, setCategories] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState('');
  const [menuItems, setMenuItems] = useState([]);
  const { addToOrder } = useContext(CartContext);

  useEffect(() => {
    const fetchCategories = async () => {
      try {
        const response = await axios.get(`${process.env.REACT_APP_API_URL}api/admin/categories`);
        setCategories(response.data);
        if (!selectedCategory && response.data.length > 0) {
          setSelectedCategory(response.data[0].name);
        }
      } catch (error) {
        console.error('Error fetching categories:', error);
      }
    };

    fetchCategories();
  }, [selectedCategory]);

  useEffect(() => {
    if (selectedCategory) {
      fetchMenuItems(selectedCategory);
    }
  }, [selectedCategory]);

  const fetchMenuItems = async (category) => {
    try {
      const response = await axios.get(`${process.env.REACT_APP_API_URL}api/commande/menuItems/${category}`);
      setMenuItems(response.data);
    } catch (error) {
      console.error('Error fetching menu items:', error);
    }
  };

  return (
    <div className="flex justify-center items-center h-screen">
      <div className="grid grid-cols-1 sm:grid-cols-3 gap-4">
        {categories.map((category) => (
          <div key={category._id}>
            <button
              className={`w-full btn hover:bg-gray-300 py-2 px-4 rounded-md ${category.name === selectedCategory ? 'btn-primary text-white' : ''}`}
              onClick={() => setSelectedCategory(category.name)}
            >
              {category.name}
            </button>
          </div>
        ))}
      </div>
      <div className="grid grid-cols-1 sm:grid-cols-3 gap-4 mt-4">
        {menuItems.map((item) => (
          <div key={item._id} className="bg-white p-4 rounded shadow-md text-center">
            <span className="block text-lg font-bold mb-2">{item.name}</span>
            <span className="block text-gray-600 mb-4">${item.price.toFixed(2)}</span>
            <button
              className="px-4 py-2 btn hover:bg-blue-300 text-white rounded"
              onClick={() => addToOrder(item)}
            >
              Ajouter au panier
            </button>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Commande;
