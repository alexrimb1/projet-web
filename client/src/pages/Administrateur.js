import React, { useState, useEffect } from 'react';

const AdminPage = () => {
  const [name, setName] = useState('');
  const [price, setPrice] = useState('');
  const [category, setCategory] = useState('');
  const [menuItems, setMenuItems] = useState([]);
  const [categories, setCategories] = useState([]);
  const [newCategory, setNewCategory] = useState('');

  useEffect(() => {
    fetchMenuItems();
    fetchCategories();
  }, []);

  const fetchMenuItems = () => {
    fetch(`${process.env.REACT_APP_API_URL}api/admin/menuItems`)
      .then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok ' + response.statusText);
        }
        return response.json();
      })
      .then(data => setMenuItems(data))
      .catch(error => console.error('Error fetching menu items:', error));
  };

  const fetchCategories = () => {
    console.log(`Fetching categories from: ${process.env.REACT_APP_API_URL}api/admin/categories`);
    fetch(`${process.env.REACT_APP_API_URL}api/admin/categories`)
      .then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok ' + response.statusText);
        }
        return response.json();
      })
      .then(data => setCategories(data))
      .catch(error => console.error('Error fetching categories:', error));
  };

  const handleAddItem = (event) => {
    event.preventDefault();
    const newItem = { name, price, category };

    fetch(`${process.env.REACT_APP_API_URL}api/admin/menuItems`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(newItem)
    })
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok ' + response.statusText);
      }
      return response.json();
    })
    .then(data => {
      console.log('Item added successfully:', data);
      setName('');
      setPrice('');
      setCategory('');
      fetchMenuItems();
    })
    .catch(error => console.error('Error adding item:', error));
  };

  const handleAddCategory = (event) => {
    event.preventDefault();
    const newCategoryData = { name: newCategory };

    fetch(`${process.env.REACT_APP_API_URL}api/admin/categories`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(newCategoryData)
    })
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok ' + response.statusText);
      }
      return response.json();
    })
    .then(data => {
      console.log('Category added successfully:', data);
      setNewCategory('');
      fetchCategories();
    })
    .catch(error => console.error('Error adding category:', error));
  };

  const handleDeleteItem = (id) => {
    fetch(`${process.env.REACT_APP_API_URL}api/admin/menuItems/${id}`, {
      method: 'DELETE'
    })
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok ' + response.statusText);
      }
      fetchMenuItems();
    })
    .catch(error => console.error('Error deleting item:', error));
  };

  const handleDeleteCategory = (categoryId) => {
    fetch(`${process.env.REACT_APP_API_URL}api/admin/categories/${categoryId}`, {
      method: 'DELETE'
    })
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok ' + response.statusText);
      }
      fetchCategories();
    })
    .catch(error => console.error('Error deleting category:', error));
  };

  return (
    <div className="flex justify-center items-center h-screen w-full bg-gray-100">
      <div className="w-full max-w-md p-6 bg-white rounded shadow-md">
        <h1 className="text-2xl font-bold mb-4 text-center">Page admin</h1>
        <form onSubmit={handleAddItem} className="mb-4">
          <input type="text" value={name} onChange={(e) => setName(e.target.value)} placeholder="Nom" className="input input-bordered input-secondary text-white mb-2" />
          <input type="number" value={price} onChange={(e) => setPrice(e.target.value)} placeholder="Prix" className="input input-bordered input-secondary text-white mb-2" />
          <div className="flex flex-col mb-2">
            <button type="submit" className="btn btn-primary w-full mb-2">Ajouter aliment</button>
            {categories.map((cat) => (
              <label key={cat._id} className="flex justify-between items-center border-b py-2">
                <input type="radio" name="category" value={cat.name} checked={category === cat.name} onChange={() => setCategory(cat.name)} />
                <span className="ml-2">{cat.name}</span>
                <button onClick={() => handleDeleteCategory(cat._id)} className="btn btn-error btn-xs">Supprimer</button>
              </label>
            ))}
          </div>
        </form>
        <form onSubmit={handleAddCategory} className="mb-4">
          <input type="text" value={newCategory} onChange={(e) => setNewCategory(e.target.value)} placeholder="Nouvelle categorie" className="input input-bordered input-secondary text-white mb-2" />
          <button type="submit" className="btn btn-success w-full">Ajouter categorie</button>
        </form>
        <ul>
          {menuItems.map((item) => (
            <li key={item._id} className="flex justify-between items-center border-b py-2">
              <span>{item.name} - ${item.price.toFixed(2)}</span>
              <button onClick={() => handleDeleteItem(item._id)} className="btn btn-error btn-xs">Supprimer</button>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default AdminPage;
