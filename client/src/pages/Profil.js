import React, { useContext, useEffect, useState } from "react";
import Log from "../components/Log";
import { UidContext } from "../components/AppContext";
import axios from "axios";

const Profil = () => {
  const uid = useContext(UidContext);
  const [user, setUser] = useState(null);

  useEffect(() => {
    const fetchUser = async () => {
      try {
        const res = await axios.get(`${process.env.REACT_APP_API_URL}api/user/${uid}`);
        setUser(res.data);
      } catch (error) {
        console.error("Error fetching user data:", error);
      }
    };

    if (uid) {
      fetchUser();
    }
  }, [uid]);

  return (
    <div className="min-h-screen bg-gray-100 py-6 flex flex-col justify-center sm:py-12">
      <div className="relative py-3 sm:max-w-xl sm:mx-auto">
        <div className="relative px-4 py-10 bg-white mx-8 md:mx-0 shadow rounded-3xl sm:p-10">
          {uid && user ? (
            <div>
              <h2 className="text-2xl font-bold mb-6">Informations de l'utilisateur :</h2>
              <div className="space-y-3">
                <p className="text-lg">Pseudo : {user.pseudo}</p>
                <p className="text-lg">Email : {user.email}</p>
                <p className="text-lg">Code-barres : {user.codeBarre}</p>
              </div>
            </div>
          ) : (
            <div className="log-container flex flex-row">
              <Log signin={false} signup={true} />
              <div className="img-container hidden md:block">
                <img src="./img/log.svg" alt="img-log" className="w-96 h-96" />
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default Profil;
