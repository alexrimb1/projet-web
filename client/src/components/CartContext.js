// CartContext.js
import React, { createContext, useState } from 'react';

export const CartContext = createContext();

export const CartProvider = ({ children }) => {
  const [order, setOrder] = useState([]);

  const addToOrder = (item) => {
    setOrder((prevOrder) => [...prevOrder, item]);
  };

  const removeOrder = () => {
    setOrder([]);
  };

  return (
    <CartContext.Provider value={{ order, addToOrder, removeOrder }}>
      {children}
    </CartContext.Provider>
  );
};
