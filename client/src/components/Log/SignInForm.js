import React, { useState } from "react";
import axios from "axios";

const SignInForm = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [method, setMethod] = useState("");
  const [codeBarre, setBarcode] = useState("");

  const handleLogin = (e) => {
    e.preventDefault();

    if (method === "scanner") {
      console.log("Connexion via le scanner de code-barre avec le code : ", codeBarre);
    } else if (method === "manual") {
      axios.post(`${process.env.REACT_APP_API_URL}api/user/login-barcode`, { codeBarre }, { withCredentials: true })
        .then((res) => {
          console.log(res);
          if (!res.data.errors) {
            window.location = "/commande";
          }
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      axios.post(`${process.env.REACT_APP_API_URL}api/user/login`, { email, password }, { withCredentials: true })
        .then((res) => {
          console.log(res);
          if (res.data.errors) {
            // Gérer les erreurs d'authentification
          } else {
            window.location = "/commande";
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  const handleShowLoginForm = () => {
    setMethod("account");
  };

  return (
    <div className="p-4 bg-white rounded-lg shadow-lg">
      {method === "account" && (
        <form onSubmit={handleLogin} id="sign-up-form">
          <div className="mb-4">
            <label htmlFor="email" className="block text-gray-700">Email</label>
            <input
              type="text"
              name="email"
              id="email"
              onChange={(e) => setEmail(e.target.value)}
              value={email}
              className="mt-1 p-2 w-full border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-200"
            />
          </div>
          <div className="mb-4">
            <label htmlFor="password" className="block text-gray-700">Mot de passe</label>
            <input
              type="password"
              name="password"
              id="password"
              onChange={(e) => setPassword(e.target.value)}
              value={password}
              className="mt-1 p-2 w-full border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-200"
            />
          </div>
          <button type="submit" className="btn mt-4 text-white py-2 btn-red px-4 rounded-md">Se connecter</button>
        </form>
      )}
      {method === "manual" && (
        <div className="mb-4">
          <input
            type="text"
            value={codeBarre}
            onChange={(e) => setBarcode(e.target.value)}
            placeholder="Saisir le code-barres"
            className="mt-1 p-2 w-full border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-200"
          />
          <button onClick={handleLogin} className="bg-indigo-500 text-white py-2 px-4 rounded-md hover:bg-indigo-600 transition-colors duration-300 mt-2">Valider</button>
        </div>
      )}
      <div className="flex justify-between">
        <button onClick={() => setMethod("scanner")} className="bg-indigo-500 text-white py-2 px-4 rounded-md hover:bg-indigo-600 transition-colors duration-300 mr-2">Scanner</button>
        <button onClick={() => setMethod("manual")} className="bg-indigo-500 text-white py-2 px-4 rounded-md hover:bg-indigo-600 transition-colors duration-300 mr-2">Taper à la main</button>
        <button onClick={handleShowLoginForm} className="bg-indigo-500 text-white py-2 px-4 rounded-md hover:bg-indigo-600 transition-colors duration-300">Connexion avec le compte</button>
      </div>
    </div>
  );
};

export default SignInForm;
