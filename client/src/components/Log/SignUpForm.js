import React, { useState } from "react";
import axios from "axios";
import SignInForm from "./SignInForm";

const SignUpForm = () => {
  const [formSubmit, setFormSubmit] = useState(false);
  const [pseudo, setPseudo] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [controlPassword, setControlPassword] = useState("");

  const handleRegister = async (e) => {
    e.preventDefault();
    const terms = document.getElementById("terms");

    if (password !== controlPassword || !terms.checked) {
      // Gérer les erreurs de validation des champs
    } else {
      await axios.post(`${process.env.REACT_APP_API_URL}api/user/register`, {
        pseudo,
        email,
        password,
      })
      .then((res) => {
        console.log(res);
        if (!res.data.errors) {
          setFormSubmit(true);
        } else {
          // Gérer les erreurs de validation du formulaire
        }
      })
      .catch((err) => console.log(err));
    }
  };

  return (
    <>
      {formSubmit ? (
        <>
          <SignInForm />
          <span></span>
          <h4 className="success">
            Enregistrement réussi, veuillez-vous connecter
          </h4>
        </>
      ) : (
        <form onSubmit={handleRegister} className="p-4 bg-white rounded-lg shadow-lg">
          <label htmlFor="pseudo" className="block mb-2">Pseudo</label>
          <input
            type="text"
            name="pseudo"
            id="pseudo"
            onChange={(e) => setPseudo(e.target.value)}
            value={pseudo}
            className="mt-1 p-2 w-full border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-200"
          />
          <div className="pseudo error"></div>
          <label htmlFor="email" className="block mt-4 mb-2">Email</label>
          <input
            type="text"
            name="email"
            id="email"
            onChange={(e) => setEmail(e.target.value)}
            value={email}
            className="mt-1 p-2 w-full border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-200"
          />
          <div className="email error"></div>
          <label htmlFor="password" className="block mt-4 mb-2">Mot de passe</label>
          <input
            type="password"
            name="password"
            id="password"
            onChange={(e) => setPassword(e.target.value)}
            value={password}
            className="mt-1 p-2 w-full border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-200"
          />
          <div className="password error"></div>
          <label htmlFor="password-conf" className="block mt-4 mb-2">Confirmer mot de passe</label>
          <input
            type="password"
            name="password"
            id="password-conf"
            onChange={(e) => setControlPassword(e.target.value)}
            value={controlPassword}
            className="mt-1 p-2 w-full border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-200"
          />
          <div className="password-confirm error"></div>
          <div className="mt-4">
            <input type="checkbox" id="terms" className="mr-2" />
            <label htmlFor="terms">
              J'accepte les{" "}
              <a href="/commande" target="_blank" rel="noopener noreferrer" className="text-blue-500 hover:underline">
                conditions générales
              </a>
            </label>
          </div>
          <div className="terms error"></div>
          <button type="submit" className="btn mt-4 text-white py-2 btn-red px-4 rounded-md">Valider inscription</button>
        </form>
      )}
    </>
  );
};

export default SignUpForm;
