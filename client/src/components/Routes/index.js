import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import Profil from '../../pages/Profil';
import Commande from '../../pages/Commande';
import Administrateur from '../../pages/Administrateur';
import Navbar from '../Navbar';
import Cart from '../../components/Cart';
import { CartProvider } from '../../components/CartContext';

const App = () => {
  return (
    <CartProvider>
      <Router>
        <Navbar />
        <Switch>
          <Route path="/" exact component={Commande} />
          <Route path="/profil" exact component={Profil} />
          <Route path="/commande" exact component={Commande} />
          <Route path="/administrateur" exact component={Administrateur} />
          <Route path="/cart" exact component={Cart} />
          <Redirect to="/" />
        </Switch>
      </Router>
    </CartProvider>
  );
};

export default App;
