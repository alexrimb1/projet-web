import React, { useContext, useState } from 'react';
import { CartContext } from './CartContext';

const Cart = () => {
  const { order, removeOrder } = useContext(CartContext);
  const [showConfirmation, setShowConfirmation] = useState(false);

  const total = order.reduce((total, item) => total + item.price, 0).toFixed(2);

  const handlePay = () => {
    setShowConfirmation(true);
  };

  const handleConfirmPayment = () => {
    removeOrder();
    setShowConfirmation(false);
  };

  return (
    <div className="container min-h-screen mx-auto mt-8 flex flex-col items-center justify-center">
      <h2 className="text-2xl font-bold mb-4">Récapitulatif de la commande</h2>
      {order.map((item, index) => (
        <div key={index} className="flex justify-between mb-2 p-2 border-b">
          <span>{item.name}</span>
          <span className="font-bold">${item.price.toFixed(2)}</span>
        </div>
      ))}
      <div className="font-bold text-lg mt-4">
        Total: ${total}
      </div>
      <button onClick={handlePay} className="bg-green-500 text-white px-4 py-2 mt-4 rounded-md hover:bg-green-600 transition-colors duration-300">Payer</button>

      {/* Pop-up de confirmation */}
      {showConfirmation && (
        <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
          <div className="bg-white p-4 rounded-md">
            <p>Confirmez-vous le paiement ?</p>
            <div className="mt-4 flex justify-end">
              <button onClick={() => handleConfirmPayment()} className="bg-green-500 text-white px-4 py-2 rounded-md mr-2 hover:bg-green-600 transition-colors duration-300">Confirmer</button>
              <button onClick={() => setShowConfirmation(false)} className="bg-red-500 text-white px-4 py-2 rounded-md hover:bg-red-600 transition-colors duration-300">Annuler</button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Cart;
