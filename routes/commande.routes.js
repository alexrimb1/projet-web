const express = require('express');
const router = express.Router();
const commandeController = require('../controllers/commande.controller');

// Route pour récupérer les éléments de menu en fonction de la catégorie sélectionnée
router.get('/menuItems/:category', commandeController.getMenuItemsByCategory);

module.exports = router;