const router = require('express').Router();
const authController = require('../controllers/auth.controller');
const userController = require('../controllers/user.controller');


// authentification (inscription, connexion, déconnexion)
router.post('/register', authController.signUp);
router.post('/login', authController.signIn);
router.post('/login-barcode', authController.signInWithBarcode);
router.get('/logout', authController.logout);

// user 
router.get('/', userController.getAllUsers);
router.get('/:id', userController.userInfo);
router.put('/:id', userController.updateUser);
router.delete('/:id', userController.deleteUser);

module.exports = router;