const express = require('express');
const router = express.Router();
const adminController = require('../controllers/admin.controller');

// Routes pour la gestion des aliments
router.post('/menuItems', adminController.createMenuItem);
router.get('/menuItems', adminController.getAllMenuItems);
router.delete('/menuItems/:id', adminController.deleteMenuItem);

router.post('/categories', adminController.createCategory);
router.get('/categories', adminController.getAllCategories);
router.delete('/categories/:id', adminController.deleteCategory);

module.exports = router;
