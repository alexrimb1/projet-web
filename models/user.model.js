const mongoose = require('mongoose');
const { isEmail } = require('validator');
const bcrypt = require('bcrypt');

// Fonction pour générer un code-barres unique
function generateUniqueBarcode() {
    const characters = '0123456789'; // Caractères autorisés dans le code-barres
    let barcode = ''; // Initialiser le code-barres

    // Générer 15 chiffres aléatoires pour le code-barres
    for (let i = 0; i < 15; i++) {
        const randomIndex = Math.floor(Math.random() * characters.length);
        barcode += characters[randomIndex];
    }

    return barcode;
}

const userSchema = new mongoose.Schema(
    {
        pseudo: {
            type: String,
            required: true,
            minLength: 3,
            maxLength: 55,
            unique: true,
            trim: true
        },
        email: {
            type: String,
            required: true,
            minLength: 3,
            validate: [isEmail, 'Format email invalide'],
            lowercase: true,
            unique: true,
            trim: true
        },
        password: {
            type: String,
            required: true,
            minlength: 6,
            maxLength: 1024
        },
        codeBarre: {
            type: String,
            unique: true
        }
    },
    {
        timestamps: true,
    }
);

// Fonction appelée avant sauvegarde sur la BD pour crypter le mot de passe
userSchema.pre('save', async function(next) {
    const salt = await bcrypt.genSalt();
    this.password = await bcrypt.hash(this.password, salt);
    next();
});

// Fonction appelée avant sauvegarde sur la BD pour générer un code-barres unique
userSchema.pre('save', function(next) {
    // Générer un code-barres unique seulement si un n'est pas déjà défini
    if (!this.codeBarre) {
        this.codeBarre = generateUniqueBarcode();
    }
    next();
});

// Fonction de validation du mot de passe lors de la connexion de l'utilisateur
userSchema.statics.login = async function(email, password) {
    const user = await this.findOne({ email });
    if (user) {
        const auth = await bcrypt.compare(password, user.password);
        if (auth) {
            return user;
        }
        throw new Error('Mot de passe incorrect');
    }
    throw new Error('Email incorrect');
};

const UserModel = mongoose.model('User', userSchema);

module.exports = UserModel;
