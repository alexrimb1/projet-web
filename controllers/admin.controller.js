const MenuItem = require('../models/item.model');
const Category = require('../models/category.model');

// Créer un nouvel aliment
exports.createMenuItem = async (req, res) => {
  try {
    const { name, price, category } = req.body;
    const menuItem = new MenuItem({ name, price, category });
    await menuItem.save();
    res.status(201).json(menuItem);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Lire tous les aliments
exports.getAllMenuItems = async (req, res) => {
  try {
    const menuItems = await MenuItem.find();
    res.json(menuItems);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Supprimer un aliment
exports.deleteMenuItem = async (req, res) => {
    try {
      const menuItem = await MenuItem.findById(req.params.id);
      if (!menuItem) {
        return res.status(404).json({ message: 'Aliment non trouvé' });
      }
      await menuItem.deleteOne(); // Utilisez deleteOne() pour supprimer l'élément
      res.json({ message: 'Aliment supprimé' });
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: error.message });
    }
  };

// Créer une nouvelle catégorie
exports.createCategory = async (req, res) => {
  try {
    const { name } = req.body;
    const category = new Category({ name });
    await category.save();
    res.status(201).json(category);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Lire toutes les catégories
exports.getAllCategories = async (req, res) => {
  try {
    const categories = await Category.find();
    res.json(categories);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Supprimer une catégorie
exports.deleteCategory = async (req, res) => {
  try {
    const category = await Category.findById(req.params.id);
    if (!category) {
      return res.status(404).json({ message: 'Catégorie non trouvée' });
    }
    await category.deleteOne();
    res.json({ message: 'Catégorie supprimée' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: error.message });
  }
};
