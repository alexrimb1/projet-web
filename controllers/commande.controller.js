const Item = require('../models/item.model');

exports.getMenuItemsByCategory = async (req, res) => {
  const { category } = req.params;
  try {
    const menuItems = await Item.find({ category: category });
    res.status(200).json(menuItems);
  } catch (error) {
    console.error('Error fetching menu items:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};